package com.mikejohn.simpleconnection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostModel {
    @SerializedName("Site")
    @Expose
    private String site;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("elementPureHtml")
    @Expose
    private String elementPureHtml;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {      //site The site
        this.site = site;
    }

    public String getName() {
        return name;
    }

    public void setName(String site) {      //name Site name
        this.name = name;
    }

    public String getDesc() {                //site Description
        return desc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {      //name Site name
        this.link = link;
    }

    public String getElementPureHtml() {                //site Description
        return elementPureHtml;
    }

    public void setElementPureHtml() {
        this.elementPureHtml = elementPureHtml;
    }
}
package com.mikejohn.simpleconnection;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GetWeatherAsync.Callback {
    public TextView tv;
    Button btn;
    String inputLine;
    int CONNECTION_TIMEOUT=100;
    private AsyncTask myTask;
    List <PostModel> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button) findViewById(R.id.button);
        tv = (TextView) findViewById(R.id.jsonText);
        posts = new ArrayList<>();

        new GetWeatherAsync(this).execute(stringToURL("http://jsonplaceholder.typicode.com/posts?_limit=10"));

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                myTask = new GetJSON()
////                        .execute(stringToURL("http://jsonplaceholder.typicode.com/posts?_limit=10"));
//            }
//        });
    }
protected URL stringToURL(String urlString){  // Custom method to convert string to url
        try{
            URL url = new URL(urlString);
            return url;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void callingBack(String gson) {
        tv.setText(gson);
    }

}

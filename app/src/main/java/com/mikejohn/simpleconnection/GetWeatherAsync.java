package com.mikejohn.simpleconnection;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetWeatherAsync extends AsyncTask <URL, Void, String>{

    Callback callback;
    String inputLine;

    public GetWeatherAsync(Callback callback){ //передаем колбэк в конструкторе
        this.callback = callback;
    }

    @Override
    protected String doInBackground(URL... urls) {
        URL url = urls[0];
        HttpURLConnection con = null;
        BufferedReader bufferedReader = null;

        try{
            con = (HttpURLConnection) url.openConnection();
            con.connect();
            InputStream in = con.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuffer stringBuffer = new StringBuffer();
            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(inputLine);
            }
            return stringBuffer.toString();

        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(String result) {
        //tv.setText(result);
        super.onPreExecute();
        callback.callingBack(result);
    }

    public interface Callback {
        void callingBack (String gson);
    }
}

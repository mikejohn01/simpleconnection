package com.mikejohn.simpleconnection;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitActivity extends AppCompatActivity {
    public static TextView tvR;
    Button btnR;
    List<PostModel> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        btnR = (Button) findViewById( R.id.buttonR);
        tvR = (TextView) findViewById(R.id.jsonTextR);
        posts = new ArrayList<>();

        //пример хабра на https://umorili.herokuapp.com/api/get?name=bash&num=50
        //App.getApi().getData("bash", 50).enqueue(new Callback<List<PostModel>>() {
        App.getApi().getData("posts?_limit", 10).enqueue(new Callback<List<PostModel>>() {
            @Override
            public void onResponse(Call<List<PostModel>> call, Response<List<PostModel>> response){
                //данные успешно пришли, но надо проверить response.body на null
                posts.addAll(response.body());
                // Когда сделаю Recycler View
                // recyclerView.getAdapter().notifyDataSetChamged();
            }
            @Override
            public void onFailure(Call<List<PostModel>> call, Throwable throwable) {
                //Произошла ошибка
                Toast.makeText(RetrofitActivity.this, "Error occured during networking", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

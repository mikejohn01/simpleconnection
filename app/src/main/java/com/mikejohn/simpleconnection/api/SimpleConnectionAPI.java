package com.mikejohn.simpleconnection.api;

import com.mikejohn.simpleconnection.PostModel;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SimpleConnectionAPI {
    // описываем аннотации

    @GET ("/api/get")
    Call<List<PostModel>> getData(@Query("name") String resourceName, @Query("num") int count);
    //метод getData, возвращающий объект типа Call<List<PostModel>>
    //методы всегда должны возвращать объекты типа Call<T>
    //PostModel - класс, сгенерированный

    //@Query("name") String resourceName - значит, что в качестве параметра запроса ставим name=<Значение строки resourceName>
    // например: https://umorili.herokuapp.com/api/get?name=bash&num=50, где name=bash&num=50 — параметры

    // Если используется алиес:
    //Полный URL для получения списка репозиториев определенного пользователя можно представить в виде
    // https://api.github.com/users/octocat/repos, где:
    //api.github.com — базовая часть адреса (всегда оканчивается слешем)
    //users/{user}/repos — метод (адрес документа, целевой адрес), где определенного пользователя (octocat) мы заменили на алиас
    //чтобы заместо алиаса поставить значение, нам нужно в параметрах функции написать
    // @Path("<Название аласа>") SomeType variable, где SomeType — любой тип (например, String, int, float).
}

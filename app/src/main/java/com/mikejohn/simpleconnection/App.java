package com.mikejohn.simpleconnection;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.mikejohn.simpleconnection.api.SimpleConnectionAPI;

public class App extends Application {
    //иниализация Ретрофита и объекта интерфейса

    private static SimpleConnectionAPI simpleConnectionAPI;
    private Retrofit retrofit;
    @Override
    public void onCreate (){
        super.onCreate();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com") //базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //конвертер для преобразования JSON'а в объект
                .build();
        simpleConnectionAPI = retrofit.create(SimpleConnectionAPI.class); //создаем объект, при помощи
        // которого будем выполнять запросы
    }
    public static SimpleConnectionAPI getApi() {
        return simpleConnectionAPI;
    }
}
